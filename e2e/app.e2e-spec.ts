import { IntegrifyPage } from './app.po';

describe('integrify App', function() {
  let page: IntegrifyPage;

  beforeEach(() => {
    page = new IntegrifyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
